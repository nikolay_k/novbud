/**
 * Author: Nikolay Kovalenko
 * Date: 12.09.2017
 * Email: nikolay.arkadjevi4@gmail.com
 * */

var Novbud = {

    init: function () {

        try {
            this.toTop();
            this.dropdownF();
        } catch (err) {
            console.log(err);
        }

    },

    counterOnScroll: function (a) {
        var oTop = $('#counter').offset().top - window.innerHeight;

        if (a === 0 && $(window).scrollTop() > oTop) {
            $('.novbud__counter-value').each(function () {
                var $this = $(this),
                    countTo = $this.attr('data-count');
                $({
                    countNum: $this.text()
                }).animate({
                        countNum: countTo
                    },
                    {
                        duration: 2000,
                        easing: 'swing',
                        step: function () {
                            $this.text(Math.floor(this.countNum));
                        },
                        complete: function () {
                            $this.text(this.countNum);
                            //alert('finished');
                        }
                    });
            });
            a = 1;
        }
    },

    toTop: function () {
        $('#toTop').bind("click", function (e) {
            e.preventDefault();
            e.stopPropagation();
            var anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: 0
            }, 1000);
            e.preventDefault();
        });

    },

    dropdownF: function () {
        $('.dropdown-toggle').bind("click", function (e) {
            e.preventDefault();
            e.stopPropagation();
            var parent = $(this).closest('.dropdown');
            $(this).toggleClass('active');
            parent.find('.dropdown-menu').fadeToggle();
            e.preventDefault();
        });

    }


};

$(document).ready(
    function () {
        Novbud.init();
        try {
            Novbud.counterOnScroll(0);
        } catch (err) {
            console.log(err);
        }

        new WOW().init({
            mobile: false
        });
    }
);

$(window).scroll(
    function () {
        try {
            Novbud.counterOnScroll(0);
        } catch (err) {
            console.log(err);
        }
    }
);

$(window).resize(function () {

});

